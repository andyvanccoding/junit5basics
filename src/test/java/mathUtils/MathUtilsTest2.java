package mathUtils;

import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assumptions.assumeTrue;

@DisplayName("When running MathUtils")
class MathUtilsTest2 {

    MathUtils mathUtils;
    TestInfo testInfo;
    TestReporter testReporter;

    @BeforeEach
    void beforeAllInit() {
        System.out.println("This needs to run before each");
    }

    @BeforeEach
    void init(TestInfo testInfo, TestReporter testReporter) {
        this.testInfo = testInfo;
        this.testReporter = testReporter;
        mathUtils = new MathUtils();
        testReporter.publishEntry("Running " + testInfo.getDisplayName() + " with tags " + testInfo.getTags());
    }

    @AfterEach
    void cleanUp() {
        System.out.println("Cleaning up...");
    }

    //group tests together
    @Nested
    @DisplayName("the add method")
    @Tag("Math")
    class AddTest {
        @Test
        @DisplayName("when adding two positive numbers")
        void addPositive() {
            assertEquals(2, mathUtils.add(1, 1), "should return the right sum.");
        }

        @Test
        @DisplayName("when adding two negative numbers should return a negative number")
        void addNegative() {
            assertEquals(-2, mathUtils.add(-1, -1), "it did not return a correct result");
        }
    }

    @Test
    @DisplayName("Substract")
    @Tag("Math")
    @Disabled
    void subtract() {
        int expected = -2;
        int actual = mathUtils.subtract(1, -3);
        //the message gets executed when the class is instantiated although it may not be used
        assertEquals(expected, actual, "Should return substraction " + expected + " but returns " + actual);
        //the make this a lazy load it can be converted to a lambda, the function will then only run on fail
        //good for expensive calculations
        assertEquals(expected, actual, () -> "Should return substraction " + expected + " but returns " + actual);

    }

    @RepeatedTest(3)
    @Tag("Circle")
    void computeCircleArea(RepetitionInfo repetitionInfo) {
        repetitionInfo.getCurrentRepetition();
        assertEquals(314.1592653589793, mathUtils.computeCircleArea(10), "should return right circle area");
    }

    @Test
    @Disabled
    void divide() {
        boolean isServerUp = false;
        assumeTrue(isServerUp);
        assertThrows(ArithmeticException.class, () -> mathUtils.divide(1, 0), "Divide by zero should throw");
    }

    @Test
    @DisplayName("Multiply method")
    @Tag("Multiply")
    void multiply() {
//        System.out.println("Running " + testInfo.getDisplayName() + " with tags " + testInfo.getTags());
        testReporter.publishEntry("Running " + testInfo.getDisplayName() + " with tags " + testInfo.getTags());
        assertAll(
                () -> assertEquals(4, mathUtils.multiply(2, 2)),
                () -> assertEquals(0, mathUtils.multiply(2, 0)),
                () -> assertEquals(-2, mathUtils.multiply(2, -1))
        );
    }

    @Test
    @Disabled
    @DisplayName("TDD should not run")
    void testDisabled() {
        fail("This test should be disabled");
    }


}