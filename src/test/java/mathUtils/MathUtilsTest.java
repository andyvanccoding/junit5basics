package mathUtils;

import org.junit.jupiter.api.*;
import org.junit.jupiter.api.condition.EnabledOnOs;
import org.junit.jupiter.api.condition.OS;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assumptions.*;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;
import org.junit.jupiter.params.provider.ValueSource;

//per default an instance gets created per method
//@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class MathUtilsTest {

    MathUtils mathUtils;

    //must be static, because an instance must be created before you can execute something
    //if @TestInstance(TestInstance.Lifecycle.PER_CLASS) is used, you don't need to make this method static
    //since it is only created once the will not throw exception
    //so @BeforeAll only executes on first creation.
//    @BeforeAll
    @BeforeEach
    void beforeAllInit() {
        System.out.println("This needs to run before each");
    }

    @BeforeEach
    void init() {
        mathUtils = new MathUtils();
    }

    @AfterEach
    void cleanUp() {
        System.out.println("Cleaning up...");
    }

    //group tests together
    @Nested
    class AddTest {
        @Test
        @DisplayName("Testing add method positive")
        void addPositive() {
            assertEquals(2, mathUtils.add(1, 1), "Check the add method");
        }

        @Test
        @DisplayName("Testing add method negative")
        void addNegative() {
            assertEquals(-2, mathUtils.add(-1, -1), "Check the add method");
        }
    }

    @DisplayName("testValueSource")
    @ParameterizedTest
    @ValueSource(ints = {1, 5, 6})
    void valueSource(int candidate) {
        System.out.println("This prints: " + candidate);
    }

    @ParameterizedTest
    @CsvFileSource(resources = "/list.csv", numLinesToSkip = 1)
    void csvSource(String year, String make) {
        System.out.println("year: " + year + " make: " + make);
    }

    @Test
    void computeCircleArea() {
        assertEquals(314.1592653589793, mathUtils.computeCircleArea(10), "should return right circle area");
    }

    @Test
    @Disabled
    void divide() {
        boolean isServerUp = false;
        assumeTrue(isServerUp);
        assertThrows(ArithmeticException.class, () -> mathUtils.divide(1, 0), "Divide by zero should throw");
    }

    @Test
    @DisplayName("Multiply method")
    void multiply() {
        assertAll(
                () -> assertEquals(4, mathUtils.multiply(2, 2)),
                () -> assertEquals(0, mathUtils.multiply(2, 0)),
                () -> assertEquals(-2, mathUtils.multiply(2, -1))
        );
    }

    @Test
    @Disabled
    @DisplayName("TDD should not run")
    void testDisabled() {
        fail("This test should be disabled");
    }


}